defmodule Game.Server do
  use GenServer

  defmodule State do
    defstruct player_manager:    nil,
              player_supervisor: nil,
              game_supervisor:   nil
    end

  # Public API

  def start_link do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def register_player(player_id) do
    GenServer.call __MODULE__, {:register_player, player_id}
  end

  def get_player(player_id) do
    GenServer.call __MODULE__, {:get_player, player_id}
  end

  def new_game(p1_id, p2_id) do
    GenServer.call __MODULE__, {:new_game, p1_id, p2_id}
  end

  # GenServer implementation

  def init(_) do
    player_manager    = start_link_or_fail(&Game.PlayerManager.start_link/0)
    player_supervisor = start_link_or_fail(&Game.PlayerSupervisor.start_link/0)
    game_supervisor   = start_link_or_fail(&Game.GameSupervisor.start_link/0)
    {:ok, %State{player_manager:    player_manager, 
                 player_supervisor: player_supervisor, 
                 game_supervisor:   game_supervisor}}
  end

  defp start_link_or_fail(f) do
    case f.() do
      {:ok, result} -> result
      other -> raise inspect other
    end
  end

  def handle_call({:register_player, player_id}, _from, state) do
    reply = do_register_player(player_id, state.player_manager, state.player_supervisor)
    {:reply, reply, state}
  end

  def handle_call({:get_player, player_id}, _from, state) do
    reply = do_get_player(player_id, state.player_manager)
    {:reply, reply, state}
  end

  def handle_call({:new_game, p1_id, p2_id}, _from, state) do
    reply = do_new_game(p1_id, p2_id, state.player_manager, state.game_supervisor)
    {:reply, reply, state}
  end

  @spec do_register_player(term, pid, pid) :: {{:ok, pid} | {:error, term}}
  def do_register_player(player_id, player_manager, player_supervisor) do
     Game.PlayerManager.register_player(player_manager, 
                                        player_supervisor, 
                                        player_id)
  end

  @spec do_get_player(term, pid) :: {:ok, pid} | :error
  def do_get_player(player_id, player_manager) do
     Game.PlayerManager.get_player(player_manager,
                                   player_id)
  end

  @spec do_new_game(term, term, pid, pid) :: {:ok, pid} | {:error, term}
  def do_new_game(p1_id, p2_id, player_manager, game_supervisor) do
    case {do_get_player(p1_id, player_manager), do_get_player(p2_id, player_manager)} do
      # Both players exist - create the game
      {{:ok, p1_pid}, {:ok, p2_pid}} -> Game.GameSupervisor.new_game(game_supervisor, p1_pid, p2_pid)

      # Not! both players exist - error
      {p1_reply, p2_reply} -> 
          {:error, "One or both players are not registered: #{p1_reply}; #{p2_reply}"}
    end
  end
end
