defmodule Game.Game do
  use GenServer

  defmodule State do
    defstruct p1: nil,
              p2: nil
  end

  # Public API

  def start_link(p1, p2) do
    GenServer.start_link(__MODULE__, %State{p1: p1, p2: p2})
  end

  def attack(game, player) do
    GenServer.cast game, {:attack, player}
  end

  # GenServer implementation

  def handle_cast({:attack, p1}, state = %State{p1: p1}) do
    Game.Player.win(p1)
    Game.Player.lose(state.p2)
    {:stop, :normal, state}
  end

  def handle_cast({:attack, p2}, state = %State{p2: p2}) do
    Game.Player.win(p2)
    Game.Player.lose(state.p1)
    {:stop, :normal, state}
  end
end
