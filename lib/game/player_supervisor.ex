defmodule Game.PlayerSupervisor do
  use Supervisor

  # Public API

  def start_link do
    Supervisor.start_link(__MODULE__, [])
  end

  def new_player(player_supervisor, player_id) do
    Supervisor.start_child(player_supervisor, [player_id])
  end

  # Supervisor callbacks

  def init([]) do
    children = [worker(Game.Player, [])]
    opts = [strategy: :simple_one_for_one]
    supervise(children, opts)
  end
end
