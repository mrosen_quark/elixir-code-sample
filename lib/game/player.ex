defmodule Game.Player do
  use GenServer

  defmodule State do
    defstruct id: 0,
              wins: 0,
              losses: 0
  end

  # Public API

  def start_link(id) do
    GenServer.start_link(__MODULE__, %State{id: id}) 
  end

  def get_state(pid) do
    GenServer.call pid, :get_state
  end

  def get_id(pid) do
    GenServer.call pid, :get_id
  end

  def get_wins(pid) do
    GenServer.call pid, :get_wins
  end

  def get_losses(pid) do
    GenServer.call pid, :get_losses
  end

  def win(pid) do
    GenServer.cast pid, :win
  end

  def lose(pid) do
    GenServer.cast pid, :lose
  end

  # GenServer implementation

  def handle_call(:get_state, _from, state) do
    {:reply, state, state}
  end

  def handle_call(:get_id, _from, state) do
    {:reply, state.id, state}
  end

  def handle_call(:get_wins, _from, state) do
    {:reply, state.wins, state}
  end

  def handle_call(:get_losses, _from, state) do
    {:reply, state.losses, state}
  end

  def handle_cast(:win, state) do
    {:noreply, %State{state | wins: state.wins + 1}}
  end

  def handle_cast(:lose, state) do
    {:noreply, %State{state | losses: state.losses + 1}}
  end
end
