defmodule Game.GameSupervisor do
  use Supervisor

  # Public API

  def start_link do
    Supervisor.start_link(__MODULE__, [])
  end

  def new_game(game_supervisor, p1_pid, p2_pid) do
    Supervisor.start_child(game_supervisor, [p1_pid, p2_pid])
  end

  # Supervisor callbacks

  def init([]) do
    children = [worker(Game.Game, [], restart: :transient)]
    opts = [strategy: :simple_one_for_one]
    supervise(children, opts)
  end
end
