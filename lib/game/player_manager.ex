defmodule Game.PlayerManager do
  use GenServer

  # Public API

  def start_link do
    GenServer.start_link(__MODULE__, Exredis.start)
  end

  def register_player(player_manager, player_supervisor, player_id) do
    GenServer.call player_manager, { :register, player_supervisor, player_id }
  end

  def get_player(player_manager, player) do
    GenServer.call player_manager, {:get, player}
  end

  # GenServer implemtation

  def handle_call({:register, player_supervisor, player_id}, _from, redis) do
    reply = do_register(player_supervisor, player_id, redis)
    {:reply, reply, redis}
  end

  def handle_call({:get, player_id}, _from, redis) do
    reply = do_get_player(player_id, redis)
    {:reply, reply, redis}
  end

  # @spec do_register(pid, term, %{term => pid}) :: {{:ok, pid} | {:error, term}, %{term => pid}}
  def do_register(player_supervisor, player_id, redis) do
    case Exredis.Api.get(redis, player_id) do
      :undefined ->
        reply = {:ok, player} = Game.PlayerSupervisor.new_player(player_supervisor, player_id)
        Exredis.Api.set(redis, player_id, player) 
        reply
      _ -> {{:error, "player #{player_id} already registered"}, redis}
    end
  end

  def do_get_player(player_id, redis) do
    case Exredis.Api.get(redis, player_id) do
      :undefined -> :error
      player -> {:ok, :erlang.binary_to_term(player)}
    end
  end
end
